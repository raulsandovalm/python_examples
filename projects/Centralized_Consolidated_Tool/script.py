import os
import pandas as pd

########## global variables [START] ##########
scriptPath = os.path.abspath('')
inputPath = scriptPath + '\\INPUT\\'
outputPath = scriptPath + '\\OUTPUT\\'
tmpPath = scriptPath + '\\TMP\\'
########## global variables  [END]  ##########

########## checkIfExists(pPath) [START] ##########
def checkIfExists(pPath):
    return os.path.isfile(pPath)
########## checkIfExists(pPath)  [END]  ##########

########## splitFileByColumnValue(pColumnName,pInputPath,pOutPath) [START] ##########
def splitFileByColumnValue(pColumnName,pInputPath,pOutPath):
    files = os.listdir(pInputPath)
    df = pd.DataFrame()
    for file in files:
        df = df.append(pd.read_excel(inputPath + file), ignore_index=True)
        uniqueValues = df[pColumnName].unique()
        # print("########## [START] ##########")
        print("Processing file: " + file)
        for value in uniqueValues:
            tmp_df = df.loc[df[pColumnName] == str(value)]
            if (checkIfExists(pOutPath + str(value) + ".xlsx")):
                tmp_df = tmp_df.append(pd.read_excel(pOutPath + str(value) + ".xlsx"), ignore_index=False)
            tmp_df = tmp_df.drop_duplicates()
            tmp_df.to_excel(pOutPath + str(value) + '.xlsx',index=None)
            tmp_df[tmp_df[pColumnName] != str(value)]
    if(checkIfExists(pOutPath + "nan.xlsx")):
        os.remove(pOutPath + "nan.xlsx")
########## splitFileByColumnValue(pColumnName,pInputPath,pOutPath)  [END]  ##########

########## appendAllFiles(pInputPath,pOutPath) [START] ##########
def appendAllFiles(pInputPath,pOutPath):
    files = os.listdir(pInputPath)
    df = pd.DataFrame()
    for file in files:
        print("Processing file: " + file)
        df = pd.read_excel(pInputPath + file)
        moda = df.loc[:,"BRANCH"].mode(dropna=True)
        moda = moda[0]
        df['MODA'] = df['BRANCH'].apply(lambda x: 1 if  x == moda else 0)
        df.sort_values(by=['NUMERO DE PARTE','MODA'], ascending=[True,True], inplace=True)
        df = df.drop_duplicates(['RFC','NUMERO DE PARTE'], keep='last')
        append_file = df.reindex(columns=['ID CLIENTE', 'CLIENTE', 'RFC', 'CVE_CLIENTE', 'ID PROVEEDOR', 'PROVEEDOR', 'NUMERO DE PARTE', 'FRACCION 2012', 'FRACCION 2020', 'NICO', 'DESCRIPCION NICO', 'DESCRIPCION DE FACTURA', 'DESCRIPCION CLASIFICACION','BRANCH','MODA'])
        append_file = append_file.drop(columns=['MODA'])
        if(checkIfExists(pOutPath + "append.xlsx")):
            append_file = append_file.append(pd.read_excel(pOutPath + "append.xlsx"), ignore_index=False)
        append_file.to_excel(pOutPath + 'append.xlsx',index=None)
########## appendAllFiles(pInputPath,pOutPath)  [END]  ##########

########## main() [START] ##########
def main():
    print("########## Split input files by client [START] ##########")
    splitFileByColumnValue('RFC',inputPath,tmpPath)
    print("########## Append files and remove duplicated [START] ##########")
    appendAllFiles(tmpPath,outputPath)
########## main()  [END]  ##########

if __name__ == "__main__":
    main()
