import requests
from bs4 import BeautifulSoup

# request data from webpage and save on variable
url = "https://scrapingclub.com/exercise/list_basic/?page=1"
response = requests.get(url)

# get the HTML part of the response from webpage
soup = BeautifulSoup(response.text,'lxml')

# items = soup.find_all('div', class_='col-lg-4 col-md-6 mb-4')

# count = 1
# for item in items:
#     itemName = item.find('h4', class_='card-title').text.strip('\n')
#     itemPrice = item.find('h5').text
#     print('%s Item name: %s Price: %s' % (count,itemName,itemPrice))
#     count = count + 1

# get data from pagination tab

#-------------------- Try by my self --------------------
# first count all pagination links
# pages = soup.find_all('li', class_='page-item')
# pages_count = 0
# for page in pages:
#     pages_count = pages_count + 1

# pages_count = pages_count - 1

# print("Total pages: %s" % (pages_count) )
# count = 1
# for i in  range(0,pages_count):
#     url = "https://scrapingclub.com/exercise/list_basic/?page=" + str(i+1)
#     print(url)
#     response = requests.get(url)

#     # get the HTML part of the response from webpage
#     soup = BeautifulSoup(response.text, 'lxml')

#     items = soup.find_all('div', class_='col-lg-4 col-md-6 mb-4')

#     for item in items:
#         itemName = item.find('h4', class_='card-title').text.strip('\n')
#         itemPrice = item.find('h5').text
#         print('%s Item name: %s Price: %s' % (count, itemName, itemPrice))
#         count = count + 1

#-------------------- Tutorial code --------------------
pages = soup.find('ul', class_='pagination')
urls = []
links = pages.find_all('a', class_='page-link')
for link in links:
    pageNum = int(link.text) if link.text.isdigit() else None
    if pageNum != None:
        x = link.get('href')
        urls.append(x)

print(urls)

count = 1
for i in urls:
    newUrl = url + i
    response = requests.get(newUrl)
    soup = BeautifulSoup(response.text, 'lxml')
    items = soup.find_all('div', class_='col-lg-4 col-md-6 mb-4')
    for item in items:
        itemName = item.find('h4', class_='card-title').text.strip('\n')
        itemPrice = item.find('h5').text
        print('%s Item name: %s Price: %s %s' % (count,itemName,itemPrice,i))
        count = count + 1
