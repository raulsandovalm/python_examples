import pandas as pd
from openpyxl.workbook import Workbook

df_csv = pd.read_csv('Names.csv', header=None)
df_csv.columns = ['First', 'Last', 'Address', 'City', 'State', 'Area Code', 'Income']

# filter dataframe and show only records where city is Riverside
# print(df_csv.loc[df_csv['City'] == 'Riverside'])

# filter dataframe and show only records where city is Riverside and First is John
# print(df_csv.loc[(df_csv['City'] == 'Riverside') & (df_csv['First'] == 'John')])

#Lambda functions

#Calculated Tax column used lambda function
df_csv['Tax %'] = df_csv['Income'].apply(lambda x: .15 if 10000 < x < 40000 else .20 if 40000 < x < 80000 else .25)
# print(df_csv)

df_csv['Tax Owed'] = df_csv['Income'] * df_csv['Tax %']
# print(df_csv)

#drop columns

#select wich column want to drop
drop_columns = ['Area Code', 'First', 'Address']
#drop columns
df_csv.drop(columns=drop_columns, inplace=True)
# print(df_csv)

#loc function to determine colum value
df_csv['Test col'] = False
df_csv.loc[df_csv['Income'] < 60000,'Test col'] = True
# print(df_csv)

#group function based on one column and average other columns
# print(df_csv.groupby(['Test col']).mean())

#group function based on one column, average other columns and sort by one column
print(df_csv.groupby(['Test col']).mean().sort_values('Income'))
