import pandas as pd
import numpy as np
from openpyxl.workbook import Workbook

df_csv = pd.read_csv('Names.csv', header=None)
df_csv.columns = ['First', 'Last', 'Address', 'City', 'State', 'Area Code', 'Income']

#drop column
df_csv.drop(columns='Address', inplace=True)

# set column as index
df_csv = df_csv.set_index('Area Code')

#print data searching by unique identifiers
print(df_csv.loc[8074])