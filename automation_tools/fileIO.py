# open file to read data
myFile = open('inputFile.txt','r')

# open file to write data
passFile = open('passFile.txt','w')
failFile = open('failFile.txt','w')

# print all file
# print(myFile.read())

# iterate file
# count = 0
# for line in myFile:
#     print(str(count)+ ".- " + line)
#     count = count + 1

# iterate file if condition
# count = 0
# for line in myFile:
#     line_split = line.split()
#     if line_split[2] == 'P':
#         print(str(count) + ".- " + line)
#         count = count + 1

# take data from one file and if condition saved as new file
# count = 1
# for line in myFile:
#     line_split = line.split()
#     if line_split[2] == 'P':
#         passFile.write(str(count) + ".- " + line)
#         count = count + 1
    
# take data from one file and saved in two diferents files depend on condition
for line in myFile:
    line_split = line.split()
    if line_split[2] == 'P':
        passFile.write(line)
    else:
        failFile.write(line)

# close file after used
myFile.close()
passFile.close()
failFile.close()